#!/usr/bin/env sh

set -eux

sudo apt install ansible --no-install-recommends

ansible-playbook ./provision.yml

